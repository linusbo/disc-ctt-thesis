% Sample file on how to use subfiles.
\documentclass[main.tex]{subfiles}

\begin{document}

\section{A representation of the Grothendieck group in cubical type theory}

\subsection{Quotient types}
\temp{
Files: setquotient2.ctt
}

A quotient type |A/R| of a type |A| and an equivalence relation |R| is a construction where the equality relation of |A| has been redefined to coincide with |R|.

\subsubsection{Propositions}
The type |hProp| is \(\Sigma\)-type which consists of a type and a proof that the type is mere proposition
\begin{lstlisting}
hProp : U
  = (X : U) * (prop X)
\end{lstlisting}

The proposition that a type is inhabited can not contain any superfluous information
\begin{lstlisting}
ishinh_UU (X : U) : U
  = (P : hProp) -> ((X -> P.1) -> P.1)
\end{lstlisting}
Here the proposition that a type is inhabited is represented by a function which given a specific property |P| takes a function of type |X -> P.1| as argument and returns an element of |P.1| as result. Since |P| is an element of |hProp|, and can as such only embed 1 bit of information which means that no information about the actual inhabitants of |X| is revealed.
That proposition that a type is inhabited is a mere proposition \temp{(proof omitted for now)}
\begin{lstlisting}
propishinh (X : U) : prop (ishinh_UU X)
  = undefined
\end{lstlisting}
This allows us to give
\begin{lstlisting}
ishinh (X : U) : hProp
  = (ishinh_UU X, propishinh X)
\end{lstlisting}

To prove that a type is inhabited, it is enough to give a single inhabitant
\begin{lstlisting}
hinhpr (X : U) : X -> (ishinh X).1
  = \ (x : X) (P : hProp) (f : X -> P.1) -> f x
\end{lstlisting}

\subsubsection{Subtypes}

A subtype consists of a function which tags each element of the main type with a value which signifies whether it is included in the subtype or not.

The type |hProp| is \(\Sigma\)-type which consists of a type and a proof that the type is mere proposition
\begin{lstlisting}
hProp : U
  = (X : U) * (prop X)
\end{lstlisting}
The type |hProp| is used in the same manner as truth values are used. 

Using |hProp| as tags, the type of subtypes of |A : U| is
\begin{lstlisting}
hsubtypes (A : U) : U
  = A -> hProp
\end{lstlisting}
As such, given a subtype |h : hsubtypes A| and an element |a : A|, the tag |t : hProp = h a| is a type which indiciates whether |a| is included in the subtype or not. Specifically, if |t| is the the empty type then |a| is not included, where as if |t| is not the empty type then |a| is included.

An element of a subtype is referred to as a carrier
\begin{lstlisting}
carrier (X : U) (A : hsubtypes X) : U
  = (x : X) * ((A x).1)
\end{lstlisting}
that is: if |A| is a subtype of |X|, then a carrier of |A| is a dependent pair consisting of an element |x : X| and a proof that |x| is an element in |A|. 

\subsection{Relations}

A relation on a type |A| is a binary function
\begin{lstlisting}
hrel (A : U) : U
  = (a b : A) -> hProp
\end{lstlisting}
which tags each pair of elements |(a, b)| with a |hProp| to indicate whether |a| are equal to |b| or not.

In order for a relation to qualify as an equivalence relation, it must be reflective, symmetric and transitive:
\begin{lstlisting}
isrefl (X : U) (R : hrel X) : U
  = (x : X) -> (R x x).1

issymm (X : U) (R : hrel X) : U
  = (x1 x2 : X) -> (R x1 x2).1 -> (R x2 x1).1

istrans (X : U) (R : hrel X) : U
  = (x1 x2 x3 : X) -> (R x1 x2).1 -> (R x2 x3).1 -> (R x1 x3).1

iseqrel (X : U) (R : hrel X) : U
  = and (and (istrans X R) (isrefl X R)) (issymm X R)
\end{lstlisting}
Using \(\Sigma\)-types, the type of equivalence relations is given as
\begin{lstlisting}
eqrel (X : U) : U
  = (R : hrel) * (iseqrel X R)
\end{lstlisting}

\subsubsection{Equivalence classes}

An equivalence class of a type |X| and a relation |R|
\begin{lstlisting}
 iseqclass (X : U) (R : hrel X) (A : hsubtypes X) : U                            
   = and (and (ishinh (carrier X A)).1                                           
           ((x1 x2 : X) -> (R x1 x2).1 -> (A x1).1 -> (A x2).1))                 
           ((x1 x2 : X) -> (A x1).1 -> (A x2).1 -> (R x1 x2).1)
\end{lstlisting}
consists of a subtype |A| of |X| which satisfies three properties. Firstly, the equivalence class is inhabited. Secondly, if two elements |x1 x2 : X| which are |R|-related to each other, then if |x1| is a member of the subtype |A| then it follow that |x2| is a member of the subtype |A| as well. Finally, if |x1 x2 : X| and both are members of the subtype |A|, then it follows that they are |R|-related.

\subsubsection{setquot}

The setquotient is represented by its equivalence classes 
\begin{lstlisting}
setquot (X : U) (R : hrel X) : U
  = (A : hsubtypes X) * (iseqclass X R A)
\end{lstlisting}

An element |x : X| can be projected into its corresponding equivalence class
\begin{lstlisting}
setquotpr (X : U) (R : eqrel X) (X0 : X) : setquot X R.1                        
  = (A, ((p1,p2),p3))                                                           
  where                                                          
  rax : isrefl X R.1                    
    = R.2.1.2 
  sax : issymm X R.1                   
    = R.2.2                    
  tax : istrans X R.1                  
    = R.2.1.1                  
  A : hsubtypes X                      
    = \(x : X) -> R.1 X0 x             
  p1 : (ishinh (carrier X A)).1        
    = hinhpr (carrier X A) (X0,rax X0) 
  p2 (x1 x2 : X) (X1 : (R.1 x1 x2).1) (X2 : (A x1).1) : (A x2).1 
    = tax X0 x1 x2 X2 X1                                         
  p3 (x1 x2 : X) (X1 : (A x1).1) (X2 : (A x2).1) : (R.1 x1 x2).1 
    = tax x1 X0 x2 (sax X0 x1 X1) X2   
\end{lstlisting}
Here the element |X0| is applied to the relation |R| to create a unary function of type |X -> hProp| which is used as the subtype. The properties that an equivalence relation needs to satisfy follows directly from the fact that the supplied relation is an equivalence relation. Furthermore, that the subtype |A| is inhabited follows immediately from its construction.

\subsubsection{The universal property of the quotient}

A function |f : A -> B| is said to respect a relation |R : hrel A| if for |a b : A| where |(R a b).1| implies that |Id B (f a) (f b)|
\begin{lstlisting}
funresprel (A B : U) (f : A -> B) (R : hrel A) : U                              
  = (a a' : A) (r : (R a a').1) -> Id B (f a) (f a')
\end{lstlisting}

The universal property of the quotient says that if |f : A -> B| respects the relation |R : hrel A|, then |f| can be factored through the the quotient |setquot A R| via the canonical projection |setquotpr|, referred to as \(\pi\). 
\[
    \xymatrix{
        A \ar[r]^\pi \ar[dr]_f & A/R \ar[d]^{\exists!g}\\
                     & B
        }
\]
which is to say, for each |f : A -> B| that respects |R| there is a unique function |g : A/R -> B| such that \( f(a) = g(\pi(a)) \).

In order to prove the universal property, it is first shown for every equivalence class |C : setquot A R| there is a |b : B| such that |(x : carrier A C.1) -> Id B b (f x.1)| 

\begin{lstlisting}
setquotunivprop' (A B : U) (sB : set B) (R : hrel A) (f : A -> B)            
     (frr : funresprel A B f R) (c : setquot A R)                             
   : isContr ((y : B) * ((x : carrier A c.1) -> Id B y (f x.1)))              
   = let                                                                      
     T : U = (y : B) * ((x : carrier A c.1) -> Id B y (f x.1))                
     pT (a b : T) : Id T a b                                                  
       = let                                                                  
         h (x : carrier A c.1) : Id B a.1 b.1 = <i> comp (<j> B) (a.2 x @ i)  
           [ (i = 0) -> <j> a.1                                               
           , (i = 1) -> <j> b.2 x @ -j ]                                      
         p0 : Id B a.1 b.1                                                    
           = c.2.1.1 (Id B a.1 b.1, sB a.1 b.1) h                             
         p1 : IdP (<i> (x : carrier A c.1) -> Id B (p0 @ i) (f x.1)) a.2 b.2  
           = let                                                              
             P (b : B) : U                                                    
               = (x : carrier A c.1) -> Id B b (f x.1)                        
             pP (b : B) (s t : (P b)) : Id (P b) s t                           
               = <i>  \ (x : carrier A c.1) -> (sB b (f x.1) (s x) (t x)) @ i 
           in lemPropF B P pP a.1 b.1 p0 a.2 b.2                              
       in <i> (p0 @ i, p1 @ i)                                                
     h (x : carrier A c.1) : T                                                
       = (f x.1, \ (x' : carrier A c.1) -> frr x.1 x'.1 (c.2.2 x.1 x'.1 x.2 x'.2))
     y : T                 
       = c.2.1.1 (T, pT) h 
   in (y, pT y)          
\end{lstlisting}

Using the |setquotunivprop'|, the actual universal property is proved.

\begin{lstlisting}
setquotunivprop (A B : U) (sB : set B) (R : eqrel A) (f : A -> B)               
     (frr : funresprel A B f R.1)                                                
   : isContr ( (g : setquot A R.1 -> B)                                          
             * (Id (A -> B) (\ (a : A) -> g (setquotpr A R a)) f))               
   = let                                                                         
     G : U                                                                       
       = setquot A R.1 -> B                                                       
     I (g : G) : U                                                               
       = Id (A -> B) (\ (a : A) -> g (setquotpr A R a)) f                        
     pI (g : G) : prop (I g)                                                     
       = setPi A (\ (a : A) -> B) (\ (a : A) -> sB) (\ (a : A) -> g (setquotpr A R a)) f
     g : G                                                                       
       = \ (x : setquot A R.1) -> (setquotunivprop' A B sB R.1 f frr x).1.1      
     i : I g                                                                     
       = <i> \ (a : A) ->                                                         
         (setquotunivprop' A B sB R.1 f frr (setquotpr A R a)).1.2 (a, R.2.1.2 a) @ i
     eq (h : (g : G) * I g) : Id ((g : G) * I g) (g, i) h                        
       = let                                                                     
         p0 : Id G g h.1                                                         
           = <j> \ (x : setquot A R.1) -> let                                    
             P (y : setquot A R.1) : hProp                                       
               = (Id B (g y) (h.1 y), sB (g y) (h.1 y))                          
             ps (a : A) : (P (setquotpr A R a)).1                                 
               = <k> comp (<_> B) ((i @ k) a)                                    
                 [ (k = 0) -> <_> g (setquotpr A R a)                            
                 , (k = 1) -> <l> (h.2 @ -l) a ]                                 
           in setquotprop A R P ps x @ j                                         
         p1 : IdP (<i> I (p0 @ i)) i h.2                                         
           = lemPropF G I pI g h.1 p0 i h.2                                       
       in <i> (p0 @ i, p1 @ i)                                                    
   in ((g, i), eq)                                                  
\end{lstlisting}

Using the |setquotunivprop'|, it is possible to define a map over setquotients
\begin{lstlisting}
setquotmap (A B : U) (sB : set B) (R : hrel A) (f : A -> B)
    (frr : funresprel A B f R) (c : setquot A R) : B
  = (setquotunivprop' A B sB R f frr c).1.1
\end{lstlisting}
which satisfies the property that
\begin{lstlisting}
 setquotmapeq (A B : U) (sB : set B) (R : eqrel A) (f : A -> B)
     (frr : funresprel A B f R.1) (x : A)
   : Id B (setquotmap A B sB R.1 f frr (setquotpr A R x)) (f x)
   = (setquotunivprop' A B sB R.1 f frr (setquotpr A R x)).1.2 (x, R.2.1.2 x)
\end{lstlisting}


\subsection{Algebraic structures}

\temp{
Files: algstruct.ctt, mon.ctt, group.ctt
}

\begin{itemize}
    \item monoid
    \item group
\end{itemize}

\subsection{The Grothendieck group}
\temp{
Files: grothendieck.ctt
}

\begin{itemize}
    \item Construction of the Grothendieck group
    \item Universal property
\end{itemize}

\end{document}
